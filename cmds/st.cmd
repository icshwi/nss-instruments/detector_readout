# -----------------------------------------------------------------------------
# Detector Readout DEV-01
# -----------------------------------------------------------------------------
require detector_readout
require streamdevice
require mrfioc2

# -----------------------------------------------------------------------------
# Environment settings
# -----------------------------------------------------------------------------

epicsEnvSet("TOP", "$(E3_CMD_TOP)/..")

iocshLoad("./iocsh/env-init.iocsh")

epicsEnvSet("IOC", "Utg-Ymir:TS")
#epicsEnvSet("SYS",          "READ-DEV")
epicsEnvSet("SYS",          "$(IOC)")
#epicsEnvSet("DEV",          "RO1")
epicsEnvSet("DEV",          "EVR-09")
epicsEnvSet("EPICS_CMDS",   "/opt/iocs")
epicsEnvSet("MRF_HW_DB",    "evr-pcie-300dc-ess.db")
epicsEnvSet("IOCNAME",       "utg-readout-dev-01")
epicsEnvSet("MASTER",       "$(EPICS_CMDS)/$(IOCNAME)/dgro_adc")
epicsEnvSet("PARAM_PARSE",  "$(MASTER)/det_param_gen/src/param_parse.py")
epicsEnvSet("OUTPUT",       "det_param_gen/output/EPICS")
epicsEnvSet("PCI_SLOT",     "5:0.0")
epicsEnvSet("PCIID",        "$(PCI_SLOT)")
epicsEnvSet("DEVICE",       "$(DEV)")
epicsEnvSet("EVR",          "$(DEV)")

# -----------------------------------------------------------------------------
# e3 Common databases, autosave, etc.
# -----------------------------------------------------------------------------
# E3 Common databases
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")

# -----------------------------------------------------------------------------
# EVR PCI setup
# -----------------------------------------------------------------------------
iocshLoad("./iocsh/evr-pcie-300dc-init.iocsh", "S=$(IOC), DEV=$(DEV), PCIID=$(PCIID)")

#mrmEvrSetupPCI("$(EVR)", $(PCI_SLOT))
# Load EVR database
#dbLoadRecords("$(MRF_HW_DB)","EVR=$(EVR),SYS=$(SYS),D=$(DEVICE),FEVT=88.0525,PINITSEQ=0")

# -----------------------------------------------------------------------------
############# ----------- Detector Readout Interface ------------ #############
# -----------------------------------------------------------------------------
epicsEnvSet("STREAM_PROTOCOL_PATH", "$(detector_readout_DIR)/db")
epicsEnvSet("PROTO",                "ics-dg-udp.proto")
epicsEnvSet("DET_CLK_RST_EVT",      "15")
epicsEnvSet("DET_RST_EVT",          "15")
epicsEnvSet("SYNC_EVNT_LETTER",     "EvtG")
epicsEnvSet("SYNC_TRIG_EVT",        "16")
epicsEnvSet("NANO_DELTA",           "1000000000")

epicsEnvSet("DestIP1", "192.168.50.12")
epicsEnvSet("DestIP2", "192.168.50.4")

epicsEnvSet("DestPort", "65535")
epicsEnvSet("SrcPort1", "65534")
epicsEnvSet("SrcPort2", "65533")

drvAsynIPPortConfigure ("UDP1","$(DestIP1):$(DestPort):$(SrcPort1) UDP")
drvAsynIPPortConfigure ("UDP2","$(DestIP2):$(DestPort):$(SrcPort2) UDP")
# asynSetTraceMask(   UDP2, -1, 0x9)
# asynSetTraceIOMask( UDP2, -1, 0x2)
# -----------------------------------------------------------------------------
# Run the db generate script
# -----------------------------------------------------------------------------
# The generating python script is remove from st.cmd, it will be run independentlly
# system "/usr/bin/python3 $(PARAM_PARSE) $(MASTER)/param_def/"

# dbLoadRecords("evr_detector_controls.db", "SYS=$(SYS), TSEVT=$(SYNC_EVNT_LETTER), SYNC-EVNT=16, DEV=$(DEV)")
  dbLoadRecords("evr_detector_controls.db", "SYS=$(SYS), TSEVT=EvtF, SYNC-EVNT=15, EVR=EVR-09")
# iocshLoad("$(MASTER)/$(OUTPUT)/bm_params_mst.cmd", "DEV=RO1, COM=UDP1, SYS=$(SYS), PROTO=$(PROTO)")
# iocshLoad("$(MASTER)/$(OUTPUT)/bm_params_mst.cmd", "DEV=RO2, COM=UDP2, SYS=$(SYS), PROTO=$(PROTO)")
iocshLoad("./cmds/bm_params_mst.cmd", "DEV=RO2, COM=UDP2, SYS=$(SYS), PROTO=$(PROTO), EVR=EVR-09")

# Load timestamp buffer database
# iocshLoad("$(evr_timestamp_buffer_DIR)/evr_timestamp_buffer.iocsh", "CHIC_SYS=$(CHIC_SYS), CHIC_DEV=$(CHIC_DEV), CHOP_DRV=$(CHOP_DRV), SYS=$(IOC), BUFFSIZE=$(BUFFSIZE)")

# Load the sequencer configuration script
# iocshLoad("$(evr_seq_calc_DIR)/evr_seq_calc.iocsh", "DEV1=$(CHOP_DRV)01:, DEV2=$(CHOP_DRV)02:, DEV3=$(CHOP_DRV)03:, DEV4=$(CHOP_DRV)04:, SYS_EVRSEQ=$(CHIC_SYS), EVR_EVRSEQ=$(CHIC_DEV):")


iocInit()

iocshLoad("./iocsh/evr-run.iocsh", "IOC=$(IOC), DEV=$(DEV)")
# -----------------------------------------------------------------------------
# Global default values
# -----------------------------------------------------------------------------
# Set the frequency that the EVR expects from the EVG for the event clock
#dbpf $(SYS)-$(DEVICE):Time-Clock-SP 88.0525

# Set delay compensation target. This is required even when delay compensation
# is disabled to avoid occasionally corrupting timestamps.
#dbpf $(SYS)-$(DEVICE):DC-Tgt-SP 70
#dbpf $(SYS)-$(DEVICE):DC-Tgt-SP 10000

# Connect prescaler reset to event $(DET_CLK_RST_EVT)
dbpf $(SYS)-$(DEVICE):Evt-ResetPS-SP $(DET_CLK_RST_EVT)

# Connect the counters to events
dbpf $(SYS)-$(DEVICE):EvtE-SP 14
dbpf $(SYS)-$(DEVICE):EvtE-SP.OUT "@OBJ=EVR-09,Code=14"
dbpf $(SYS)-$(DEVICE):EvtF-SP $(DET_CLK_RST_EVT)
dbpf $(SYS)-$(DEVICE):EvtF-SP.OUT "@OBJ=EVR-09,Code=$(DET_CLK_RST_EVT)"
dbpf $(SYS)-$(DEVICE):$(SYNC_EVNT_LETTER)-SP $(SYNC_TRIG_EVT)
#dbpf $(SYS)-$(DEVICE):EvtG-SP.OUT "@OBJ=EVR-09,Code=$(SYNC_TRIG_EVT)"
dbpf $(SYS)-$(DEVICE):EvtG-SP.OUT "@OBJ=EVR-09,Code=15"


# Map pulser 9 to event code SYNC_TRIG_EVT
dbpf $(SYS)-$(DEVICE):DlyGen9-Evt-Trig0-SP $(SYNC_TRIG_EVT)
dbpf $(SYS)-$(DEVICE):DlyGen9-Width-SP 10

# Set up Prescaler 0
dbpf $(SYS)-$(DEVICE):PS0-Div-SP 2

# Connect FP10 to PS0
dbpf $(SYS)-$(DEVICE):OutFPUV10-Ena-SP 1
dbpf $(SYS)-$(DEVICE):OutFPUV10-Src-SP 40

# Connect FP11 to Pulser 9
dbpf $(SYS)-$(DEVICE):OutFPUV11-Ena-SP 1
dbpf $(SYS)-$(DEVICE):OutFPUV11-Src-SP 9


# Connect FP12 to PS0
dbpf $(SYS)-$(DEVICE):OutFPUV12-Ena-SP 1
dbpf $(SYS)-$(DEVICE):OutFPUV12-Src-SP 40

# Connect FP09 to PS0
dbpf $(SYS)-$(DEVICE):OutFPUV09-Ena-SP 1
dbpf $(SYS)-$(DEVICE):OutFPUV09-Src-SP 40

# Connect FP13 to Pulser 9
dbpf $(SYS)-$(DEVICE):OutFPUV13-Ena-SP 1
dbpf $(SYS)-$(DEVICE):OutFPUV13-Src-SP 9

# Map pulser 7 to event code 14
dbpf $(SYS)-$(DEVICE):DlyGen7-Evt-Trig0-SP 14
## --- Map pulser 7 (which triggers sequencer) to event 14 to model meta data trigger) ---- ##
dbpf $(SYS)-$(DEVICE):DlyGen7-Evt-Trig0-SP 14
dbpf $(SYS)-$(DEVICE):DlyGen7-Width-SP 10

# Connect FP2 to Pulser 9
dbpf $(SYS)-$(DEVICE):OutFPUV02-Ena-SP 1
dbpf $(SYS)-$(DEVICE):OutFPUV02-Src-SP 9

# Connect FP3 to Pulser 9
dbpf $(SYS)-$(DEVICE):OutFPUV03-Ena-SP 1
dbpf $(SYS)-$(DEVICE):OutFPUV03-Src-SP 9

######## load the sync sequence ######
dbpf $(SYS)-$(DEVICE):SoftSeq0-Disable-Cmd 1
dbpf $(SYS)-$(DEVICE):SoftSeq0-Unload-Cmd 1
dbpf $(SYS)-$(DEVICE):SoftSeq0-Load-Cmd 1

#Use ticks
dbpf $(SYS)-$(DEVICE):SoftSeq0-TsResolution-Sel  "0"
dbpf $(SYS)-$(DEVICE):SoftSeq0-Commit-Cmd 1

#connect the sequence to software trigger
#dbpf $(SYS)-$(DEVICE):SoftSeq0-TrigSrc-Scale-Sel "Software"

#connect the sequence to software trigger
dbpf $(SYS)-$(DEVICE):SoftSeq0-TrigSrc-Pulse-Sel "Pulser 7"

#dbpf $(SYS)-$(DEVICE):SoftSeq0-RunMode-Sel "Single"

#add sequence events and corresponding tick lists
#system "/bin/bash /epics/iocs/cmds/hzb-v20-evr-02-cmd/evr_seq_sync.sh"

#perform sync one next event 125
#dbpf $(SYS)-$(DEVICE):SoftSeq0-Enable-Cmd 1

#dbpf $(SYS)-$(DEVICE):syncTrigEvt-SP $(SYNC_TRIG_EVT)
dbpf $(SYS)-$(DEVICE):FracNsecDelta-SP 88052500

#EOF
