# Detector Readout Interface

EPICS support module for DMSC detector interface. Exposes functionality of various types. Allows user to specify register offset, scan period, pini, default, max, min and the name.
These are populated by [det_param_gen](https://github.com/icshwi/det_param_gen)

Each type has at least a read PV

`"$(SYS)-$(DEV):$(SPACE)-$(SFX)-RBV"`

and a PV to set the offset

`"$(SYS)-$(DEV):$(SPACE)-$(SFX)-OFF"`
  
additionally, there might be a write PV

`"$(SYS)-$(DEV):$(SPACE)-$(SFX)-SP"`

and other PV's which parse the RBV to make it human readable, or the SP to convert it as required for writing to one or multiple registers. 

## Dependencies

| module | version |
| ------ | ------ |
| [streamdevice](https://github.com/paulscherrerinstitute/StreamDevice) | 2.8.10 |
| [autosave ](https://github.com/epics-modules/autosave/)| 5.9.0 |

Additionally there is a dependency on [det_param_gen](https://github.com/icshwi/det_param_gen), the scripts which parse parameter files and generate .cmd files using the parameters defined in this support module.

## Protocol 

The protocol file ics-dg-udp.proto defines how communication is handled by stream device. Each protocol expects that there will be a record called 

`$(SYS)-$(DEV):$(SPACE)-$(SFX)-OFF`

which contains the register offset to use. If multiple registers are specified then 

`$(SYS)-$(DEV):$(SPACE)-$(SFX)-OFF0`

`$(SYS)-$(DEV):$(SPACE)-$(SFX)-OFF1`

`$(SYS)-$(DEV):$(SPACE)-$(SFX)-OFF2`

are expected. 

| protocol | description | use |
| ------ | ------ | ------ |
| read1Reg | read a single register and put it in the record that calls this protocol | `@$(PRO) read1Reg($(SYS)-$(DEV):$(SPACE)-$(SFX)) $(COM)` |
| read2Reg | read two registers, put the values in the records specified | `$(PRO) read2Reg($(SYS)-$(DEV):$(SPACE)-$(SFX),LW-RBV,UW-RBV) $(COM)` |
| read3Reg | read three registers, put the values in the records specified | `$(PRO) read3Reg($(SYS)-$(DEV):$(SPACE)-$(SFX),LW-RBV,MW-RBV,UW-RBV) $(COM)` |
| write1Reg | write the value from the record calling this to a single register | `@$(PRO) write1Reg($(SYS)-$(DEV):$(SPACE)-$(SFX)) $(COM)` |
| write2Reg | write the value from the records specified to two registers at offset OFF0 and OFF1| `@$(PRO) write2Reg($(SYS)-$(DEV):$(SFX),LW-SP,UW-SP) $(COM)` |
| reset | sends the protocol reset command and checks that the response confirms it's been received and carried out | `@$(PRO) reset $(COM)`

When writing, the value written is echoed back by the readout system. this is put into a record or records of the form

`$(SYS)-$(DEV):$(SPACE)-$(SFX)-ECHO`

There is currently no exception handling implemented in the protocol file.


## Types

| Parameter Type | Db  | Src | Description | 
| ------ | ------ | ------ | ------ | 
| RO | ro_register.db | | reads a single 32 bit integer |
| RW | rw_register.db | | writes to a single 32 bit integer and reads it back |
| BUILD | buildTime_register.db | getBuildTime_aSub.c | parses a single register to determine firmware build time |
| GIT | githash_register.db | printGitHash_aSub.c | parses 3 registers and forms the firmware githash |
| TS | detector_controls.db | | reads the two timestamp registers, creates general read write function  |
| RWLONG | 2rw_register.db | | reads two PV's and writes two two registers, reading them back. If either PV is changed, both are written and read. This is used in the ring-dev-01 IOC to create two registers for the timestamp, without needing to interface to the EVR which isn't included in the design |
| RESET | reset_register | | writes to a single register to reset the device. Creates a "reset then enable" PV  |
| MAC | rw_mac_address.db | convertMACAddress.cpp | parses a MAC address string, and writes to two registers. Reads them back and forms a MAC address string |
| IP | rw_ip_address.db | convertIPAddress.cpp | parses an IP address string and then writes to a register. Reads it back and forms an IP string |
| PTO | packetTimeout_register.db |  | parses an interval in ms and converts it to be written to a single register. Reads that back and parses it to a ms value |
| LED_SEL | led_sel_reg.db |  | exposes different optins for LED outputs on the Interface Demonstrator and Beam Monitor systems |
| PAYLOAD | payload_select.db |  | choose between sending packets to the DMSC that are a ramp or from the ADC |
| OVERSMP | oversampling.db | | Sets and reads back a register which controls [oversampling](https://en.wikipedia.org/wiki/Oversampling#:~:text=In%20signal%20processing%2C%20oversampling%20is,Nyquist%20rate%20or%20above%20it.) The sample rate is also determined based on $(SYS)-$(DEV):$(SPACE)-SAMP-RATE-RBV. Threshold PV's $(SYS)-$(DEV):$(SPACE)-PULSE-THRESH-00-SP are processed to take account of the new effective number of bits.   |
| CLK_SEL | clock_select.db | | Selects between interal and external clock | 
| SMP_MODE | sampling_mode.db | | selects between ramp and adc values being sent in packets to DMSC |
| THRESH | threshold.sb | | Takes a desired threshold from the user and, depending on the oversampling ratio (which determines the number of bits in the effective adc range), and the range setting of the DAQ input stage, calculates the correct value to be written to the threshold register in the firmware. The register is read back and the conversion applied again to show the user what the achieved value is. | 
| GAIN | channel_gain.db | chInp_aSub.c | Allows user to choose one of a selection of possible input ranges and impedances. Sets registers accordingly and reads them back to confirm. Additionally determines the range of the channel which is used by the THRESH type. Sets the limits of the threshold records. |
| OFFSET | ch_offset.db | | Parses user input for a channel offset in volts and determines the value to be written to the device. Configures and then sends that value over SPI. Assumes that SPI records are correctly named. See template for details. |
| WS | ws_register.db | | Write Strobe, writes a 1 to a register. No readback. |


### Creating New Types 

You might want to make a new type that parses user input and writes to one or more registers, then parses the response and displays that to a user. you'll need to:

1. Create the template file. Use wr_register.db as the basis for this, and make sure you include the $(SYS)-$(DEV):$(SPACE)-$(SFX) format for the naming
2. Create any requried asub records. 
3. Make the support module.
3. Add the type to [param_def.json](https://github.com/icshwi/det_param_gen/blob/master/param_def.json)
4. Use the type in your parameter definition file in the firmware repository. [See this example](https://bitbucket.org/europeanspallationsource/dgro_adc/src/eth_slowctl/param_def/) 
5. Check it works by running [param_parse.py](https://github.com/icshwi/det_param_gen/blob/master/src/param_parse.py) and seeing that the .cmd file includes the reference to your new parameter type. 